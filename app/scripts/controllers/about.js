'use strict';

/**
 * @ngdoc function
 * @name sillyAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sillyAppApp
 */
angular.module('sillyAppApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
