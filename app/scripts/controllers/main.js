'use strict';

angular.module('sillyAppApp')
  .controller('MainCtrl', function ($scope) {
    $scope.numbers = [0];

    for(var i=0; i < 20; i++){
      if($scope.numbers[i] === 0){
        $scope.numbers.push(1);
      }
      else if($scope.numbers[i] === 1 && i === 1){
        $scope.numbers.push(1);
      }
      else{
        $scope.numbers.push(
          $scope.numbers[i-1] + $scope.numbers[i]
        );
      }
    }

  });
